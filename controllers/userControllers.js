/*Dependencies*/
const bcrypt = require("bcrypt");

/*File directories*/
const User = require("../models/User");
const auth = require("../auth");


/*CHECK EMAIL: Check email if existing*/
	module.exports.checkEmailExists = (request, response, next) => {
		return User.find({email: request.body.email}).then(checkEmail => {
			console.log(request.body.email)

			let emailExists;

			if(checkEmail.length > 0){
				return response.send({emailExists: true});
			} else{
				next();
			}
		}).catch(error => {
			console.log(error)
			response.send(error)
		})
	}


/*REGISTER A USER: Register a user*/
	module.exports.registerUser = (request, response) => {

		let newUser = new User({
			firstName: request.body.firstName,
			lastName: request.body.lastName,
			userName: request.body.userName,
			email: request.body.email,
			password: bcrypt.hashSync(request.body.password, 5),
			mobileNo: request.body.mobileNo
		})

//		let registered;

		return newUser.save().then(register => {
			console.log(register)
			response.send({registered: true})
		}).catch(error => {
			console.log(error);
			response.send({registered: false})
		})
	}
 

/*LOGIN USER: Password encryption for registration*/
	module.exports.login = (request, response) => {
		return User.findOne({email: request.body.email}).then(checkPassword => {

			if(checkPassword === null){
				return response.send({accessToken: 'empty'});
			} else{
				const isPasswordCorrect = bcrypt.compareSync(request.body.password, checkPassword.password)

				if(isPasswordCorrect){
					let token = auth.createToken(checkPassword)
					console.log(token)
					return response.send({accessToken: token})
				} else{
					return response.send({accessToken: 'empty'});
				}
			}
		}).catch(error => {
			console.log(error)
			return response.send(error)
		})
	}


/*UPDATE ROLE: Update as admin role */
module.exports.updateRole = (request, response) => {
	const userInformation = auth.decodeToken(request.headers.authorization);

	let idToUpdate = request.params.userId;

	if(userInformation.isAdmin){
		return User.findById(idToUpdate).then(result => {

			let update = {
				isAdmin: !result.isAdmin
			}

			return User.findByIdAndUpdate(idToUpdate, update, {new: true}).then(document => {
				document.password = "**********"
				response.send(document)
			}).catch(error => {
				response.send(error)})
		}).catch(error => {
			response.send(error)})
	} else{
		return response.send("Sorry. You are not an admin, you don't have access on this page.")
	}
}	


/*Retrieve user details*/
	module.exports.profileDetails = (request, response) => {
		const userInformation = auth.decodeToken(request.headers.authorization);
		console.log(userInformation);
		return User.findById(userInformation.id).then(result => {
			result.password = "Confidential";
			return response.send(result)
		}).catch(error => {
			return response.send(error);
		})
	}
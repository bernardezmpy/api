/*Dependencies*/
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();

/*Routes*/
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require('./routes/orderRoutes');
const checkoutRoutes = require('./routes/checkoutRoutes');

/*Database connection*/
mongoose.connect("mongodb+srv://mpyb:mpyb@zuittbatch243.kotcmj5.mongodb.net/Capstone2?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

/*Catch error connection to database && Successful connection of API to database*/
mongoose.connection.on("error", console.log.bind(console, "Connection Error"));
mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas"))

/*Middlewares*/
app.use(cors());
app.use(express());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

/*API URL Parameters*/
app.use("/user", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
/*app.use("/orders", checkoutRoutes);*/

/*Check if API is working successfully*/
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online in port ${process.env.PORT || 4000}`);
})
/*Dependencies*/
const express = require("express");
const router = express.Router();

/*File directories*/
const auth = require("../auth")
const orderControllers = require("../controllers/orderControllers");
const checkoutControllers = require("../controllers/checkoutControllers");

/*Get user orders*/
router.get("/", auth.tokenVerification, orderControllers.getUserOrder);

/*Remove from cart*/
router.delete("/removeProductFromCart", auth.tokenVerification, orderControllers.removeFromCart)

/*Add to cart orders*/
router.post("/addToCart/:productId", auth.tokenVerification, orderControllers.addToCart);

//checkout order from cart
router.post("/checkout/:cartItemId", auth.tokenVerification, checkoutControllers.checkout);

module.exports = router; 
/*Dependencies*/
const express = require("express");
const router = express.Router();

/*File directories*/
const auth = require("../auth");
const userControllers = require("../controllers/userControllers");


/*Check email if existing*/
router.post("/checkEmail", userControllers.checkEmailExists);

/*Register a user*/
router.post("/register", userControllers.checkEmailExists, userControllers.registerUser);

/*Password encryption for registration*/
router.post("/loginUser", userControllers.login);

/*Retrieve user details*/
router.get("/profile", auth.tokenVerification, userControllers.profileDetails);

/*Update as admin role*/
router.patch("/updateRole/:userId", auth.tokenVerification, userControllers.updateRole);

module.exports = router;
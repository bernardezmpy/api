/*Dependencies*/
const express = require("express");
const router = express.Router();

/*File directories*/
const auth = require("../auth")
const productControllers = require("../controllers/productControllers")


/*Add products to API by Admin User*/
router.post("/", auth.tokenVerification, productControllers.addProducts);

/*Retrieve all active products (can accessed by user)*/
router.get("/allActiveProducts", productControllers.getAllActive);

/*Retrieve all products by Admin only*/
router.get("/allProducts", auth.tokenVerification, productControllers.getAllProducts);

/*Update a product by Admin only*/
router.put("/updateProduct/:productId", auth.tokenVerification, productControllers.updateProduct);

/*Retrieve single product (can access by user)*/
router.get("/:productId", productControllers.getProduct)

/*Archive a product through ID by Admin only*/
router.patch("/:productId/archived", auth.tokenVerification, productControllers.archiveProduct)

/*Unarchive a product through ID by Admin only*/
router.patch("/:productId/unarchived", auth.tokenVerification, productControllers.unarchiveProduct)

module.exports = router;
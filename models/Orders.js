/*Dependencies*/
const mongoose = require("mongoose");

/*File directories*/
const User = require("../models/User");
const Products = require("../models/Products");

const orderSchema = new mongoose.Schema({

	userId: {
		type: String,
		required: [true, "User ID is required!"]
	},

	customerName: {
		type: String,
		required: [true, "Customer name is required!"]
	},

	productId: {
		type: String,
		required: [true, "User ID is required!"]
	},

	productName: {
		type: String,
		required: [true, "Product name is required!"]
	},

	productPrice: {
		type: Number,
		required: [true, "Product price is required!"]
	},

	quantity: {
		type: Number,
		required: [true, "Quantity is required!"]
	},

	subtotal: {
		type: Number,
		required: [true, "Total amount is required!"]
	},

	status: {
		type: String,
		default: "Added to Cart"
	}
	
}, {timestamps: true})

module.exports = mongoose.model("Orders", orderSchema);